#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>

char *loadShader(const char* filename);
void initProgram(GLuint *program,
                 const char *vertexStr,
                 const char *fragmentStr);

#endif
