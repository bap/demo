#define NB_VERTEX  24

enum {DOWN=0, UP=1, LEFT=2, RIGHT=3, FRONT=4, BACK=5};
enum {SW=0, NW=1, NE=2, SE=3};
#define VERTEX_INDEX(face, pos) (face * 12 + pos * 3)

static GLfloat position[NB_VERTEX * 3] =
{
//DOWN
  1.0,  1.0, -1.0,
  1.0, -1.0, -1.0,
 -1.0, -1.0, -1.0,
 -1.0,  1.0, -1.0,

//UP
  1.0, -1.0,  1.0,
  1.0,  1.0,  1.0,
 -1.0,  1.0,  1.0,
 -1.0, -1.0,  1.0,

//LEFT
  1.0,  1.0, -1.0,
  1.0,  1.0,  1.0,
  1.0, -1.0,  1.0,
  1.0, -1.0, -1.0,

//RIGHT
 -1.0, -1.0, -1.0,
 -1.0, -1.0,  1.0,
 -1.0,  1.0,  1.0,
 -1.0,  1.0, -1.0,

//FRONT
 -1.0,  1.0, -1.0,
 -1.0,  1.0,  1.0,
  1.0,  1.0,  1.0,
  1.0,  1.0, -1.0,

//BACK
  1.0, -1.0, -1.0,
  1.0, -1.0,  1.0,
 -1.0, -1.0,  1.0,
 -1.0, -1.0, -1.0,
};

static GLfloat texture[NB_VERTEX * 2] =
{
 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,
};

static GLfloat normal[NB_VERTEX * 3] =
{
//DOWN
  0.0,  0.0, -1.0,
  0.0,  0.0, -1.0,
  0.0,  0.0, -1.0,
  0.0,  0.0, -1.0,

//UP
  0.0,  0.0,  1.0,
  0.0,  0.0,  1.0,
  0.0,  0.0,  1.0,
  0.0,  0.0,  1.0,

//LEFT
  1.0,  0.0,  0.0,
  1.0,  0.0,  0.0,
  1.0,  0.0,  0.0,
  1.0,  0.0,  0.0,

//RIGHT
 -1.0,  0.0,  0.0,
 -1.0,  0.0,  0.0,
 -1.0,  0.0,  0.0,
 -1.0,  0.0,  0.0,

//FRONT
  0.0,  1.0,  0.0,
  0.0,  1.0,  0.0,
  0.0,  1.0,  0.0,
  0.0,  1.0,  0.0,

//BACK
  0.0, -1.0,  0.0,
  0.0, -1.0,  0.0,
  0.0, -1.0,  0.0,
  0.0, -1.0,  0.0,
};

static GLfloat color[NB_VERTEX * 3] =
{
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
  1.0,   1.0,   1.0,
};

void update_position(GLuint *buf, GLfloat *position, GLsizeiptr size)
{
  glBindBuffer(GL_ARRAY_BUFFER, buf[POSITION]);
  glBufferData(GL_ARRAY_BUFFER, size, position, GL_STATIC_DRAW);
  glVertexPointer(3, GL_FLOAT, 0, 0);
}

void update_normal(GLuint *buf, GLfloat *normal, GLsizeiptr size)
{
  glBindBuffer(GL_ARRAY_BUFFER, buf[NORMAL]);
  glBufferData(GL_ARRAY_BUFFER, size, normal, GL_STATIC_DRAW);
  glNormalPointer(GL_FLOAT, 0, 0);
}

void update_texture(GLuint *buf, GLfloat *texture, GLsizeiptr size)
{
  glBindBuffer(GL_ARRAY_BUFFER, buf[TEXTURE]);
  glBufferData(GL_ARRAY_BUFFER, size, texture, GL_STATIC_DRAW);
  glTexCoordPointer(2, GL_FLOAT, 0, 0);
}

void update_color(GLuint *buf, GLfloat *color, GLsizeiptr size)
{
  glBindBuffer(GL_ARRAY_BUFFER, buf[COLOR]);
  glBufferData(GL_ARRAY_BUFFER, size, color, GL_STATIC_DRAW);
  glColorPointer(3, GL_FLOAT, 0, 0);
}

void update_cube()
{
  update_position(buf, position, sizeof (position));
  update_color(buf, color, sizeof (color));
  update_normal(buf, normal, sizeof (normal));
  update_texture(buf, texture, sizeof (texture));
}

void draw_cube(void)
{
  glDrawArrays(GL_QUADS, 0, NB_VERTEX);
}
