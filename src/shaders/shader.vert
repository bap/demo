uniform int t;
uniform float beat_p;
float time = float(t)/1000.0;

void main()
{
  vec3 normal, lightDir;
  vec4 diffuse, ambient;
  float NdotL;

  normal = normalize(gl_NormalMatrix * gl_Normal);
  lightDir = normalize(vec3(gl_LightSource[0].position));
  NdotL = max(dot(normal, lightDir), 0.0);
  diffuse = gl_Color * gl_LightSource[0].diffuse;

  ambient = gl_Color * gl_LightSource[0].ambient;
  gl_FrontColor =  NdotL * diffuse + ambient;


  gl_TexCoord[0] = gl_MultiTexCoord0;
  gl_Position = ftransform();
}
