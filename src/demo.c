#include <GL/glew.h>

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_image.h>

#include "shader.h"
#include "scenes.h"

void load_texture(GLuint *texture, const char * filename)
{
  SDL_Surface *surface;
  GLenum texture_format;
  GLint  nb_colors;

  SDL_RWops *rwop = SDL_RWFromFile(filename, "rb");

  if ((surface = IMG_LoadJPG_RW(rwop)) == NULL)
  {
    printf("SDL could not load texture %s: %s\n", filename, IMG_GetError());
    SDL_Quit();
    exit(1);
  }

  // Check that the image dimensions are powers of 2
  if (surface->w % 2 != 0 || surface->h % 2 != 0)
    printf("warning: %s dimensions are not powers of 2\n", filename);

  nb_colors = surface->format->BytesPerPixel;
  if (nb_colors == 4)
    texture_format = (surface->format->Rmask == 0xff) ? GL_RGBA : GL_BGRA;
  else if (nb_colors == 3)
    texture_format = (surface->format->Rmask == 0xff) ? GL_RGB : GL_BGR;
  else
    printf("warning: the image is not truecolor.this will probably break\n");

  glGenTextures(1, texture);
  glBindTexture(GL_TEXTURE_2D, *texture);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);



  gluBuild2DMipmaps(GL_TEXTURE_2D, nb_colors, surface->w, surface->h,
                    texture_format, GL_UNSIGNED_BYTE, surface->pixels);

  if (surface)
    SDL_FreeSurface(surface);
}

int main(int argc, char **argv)
{
#ifdef DEBUG
  SDL_SetVideoMode(WIDTH, HEIGHT, 0, SDL_OPENGL);
#else
  SDL_SetVideoMode(WIDTH, HEIGHT, 0, SDL_OPENGL | SDL_FULLSCREEN);
#endif
  SDL_ShowCursor(SDL_DISABLE);
  glewInit();

  if (glewIsSupported("GL_VERSION_2_0"))
    printf("Ready for OpenGL 2.0\n");
  else
  {
    printf("OpenGL 2.0 not supported\n");
    exit(1);
  }

  // Music
  Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);
  Mix_Music *track = Mix_LoadMUS("media/track.ogg");
  unsigned int music_start = SDL_GetTicks();
  if(track == NULL)
  {
    printf("Mix_LoadMUS(\"media/track.ogg\"): %s\n", Mix_GetError());
    return 1;
  }
  Mix_PlayMusic(track, 1);
  unsigned int music_offset = argc > 1 ? atoi(argv[1]) : 0;
  if(argc > 1 && Mix_SetMusicPosition(music_offset) == -1)
  {
    printf("Mix_SetMusicPosition: %s\n", Mix_GetError());
    return 1;
  }
  music_offset *= 1000;

  const char *vertexStr = loadShader("src/shaders/shader.vert");
  const char *fragmentStr = loadShader("src/shaders/shader.frag");

  GLuint program;
  initProgram(&program, vertexStr, fragmentStr);
  GLint offsetLocation = glGetUniformLocation(program, "resolution");
  GLint beat_p = glGetUniformLocation(program, "beat_p");
  glUseProgram(program);
  glUniform2f(offsetLocation, WIDTH, HEIGHT);
  offsetLocation = glGetUniformLocation(program, "t");

  glMatrixMode(GL_PROJECTION);
  gluPerspective(60, (double)WIDTH / HEIGHT, 0.1, 1000.0);
  glMatrixMode(GL_MODELVIEW);

  glEnable(GL_DEPTH_TEST);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);

  init_vbo();

  // Texture
  GLuint texture;
  load_texture(&texture, "media/concrete.jpg");

  unsigned int etime, last_time, music_time, time = SDL_GetTicks();
  unsigned int fps = 0;
  unsigned int last_frame = time;
  SDL_Event event;
  int done = 0;

  uint  pos;
  uint  b;
  float p;

  while (!done)
  {
      last_time = time;
      time = SDL_GetTicks();
      etime = time - last_time;
      fps++;
      if (time - last_frame >= 1000)
      {
        printf("fps: %d\n", fps);
        fps = 0;
        last_frame = time;
      }

      glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
      glLoadIdentity();

      music_time = music_offset + time - music_start;
      done = display(music_time, etime, b, p);

      glUniform1i(offsetLocation, music_time);
      pos = 0;
      b = is_on_beat(music_time, &pos);
      p = pos / BEAT_TIME;
      glUniform1f(beat_p, p);

      SDL_PollEvent(&event);
      if (event.type == SDL_KEYDOWN)
      {
        switch (event.key.keysym.sym)
        {
          case SDLK_ESCAPE: done = 1; break;
          case SDLK_q: done = 1; break;
          default: break;
        }
      }
      SDL_GL_SwapBuffers();
    }

  glDeleteTextures(1, &texture);
  delete_vbo();
  Mix_FreeMusic(track);
  Mix_CloseAudio();
  SDL_Quit();

  return 0;
}
