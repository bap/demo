#include "scenes.h"

#include <GL/glew.h>

#include <math.h>
#include <stdio.h>

# define PHASE_1 (time < 54800)
# define PHASE_2 (time >= 54800 && time < 100000)

static const uint beats[] = {6900, 13700, 20600, 27400, 34300, 41200, 54800,
                             56700,58400, 60100, 61800, 63500, 68700, 70300,
                             72100, 73800, 75500, 77200, 82300, 84000, 85700,
                             87500, 89200, 90900};

#define NB_BUFFERS 4
static GLuint buf[NB_BUFFERS];
enum
{
  POSITION = 0,
  COLOR = 1,
  NORMAL = 2,
  TEXTURE = 3
};

#include "cube.c"

void init_vbo(void)
{
  glGenBuffers(NB_BUFFERS, buf);
  update_cube();
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}
void delete_vbo(void)
{
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_NORMAL_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDeleteBuffers(NB_BUFFERS, buf);
}

uint is_on_beat(uint time,
                uint *pos)
{
  for (uint i = 0; i < sizeof (beats) / sizeof (beats[0]); i++)
  {
    if (time - beats[i] > 0 && time - beats[i] < BEAT_TIME)
    {
      *pos = time - beats[i];
      return (i + 1);
    }
  }

  return 0;
}

static void tree_rec(float size, uint time)
{
  if (size < 0.1)
    return;

  glPushMatrix();
  draw_cube();
  glTranslatef(0.0, 0.0, 1.5);
  glScalef(0.8, 0.8, 0.8);
  glRotatef(10, 0, 0, 1);
  glRotatef(sin(time/500.0)*10, 1.0, 1.0, 0.0);
  tree_rec(size*0.8, time);
  glPopMatrix();
}

static void tree(uint time)
{
  tree_rec(5, time);
  glRotatef(90, 1, 0, 0);
  tree_rec(5, time);
  glRotatef(90, 0, 1, 0);
  tree_rec(5, time);
  glRotatef(90, 0, 1, 0);
  tree_rec(5, time);
  glRotatef(90, 0, 1, 0);
  tree_rec(5, time);
  glRotatef(90, 1, 0, 0);
  tree_rec(5, time);
}

static void scene1(uint time, uint etime, uint b, float p)
{
  static float angle = 0.0;

  // Background
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0.0, 1.0, 0.0, 1.0);
  glDisable(GL_DEPTH_TEST);
  glColor3f(0,1,0);
  glBegin(GL_QUADS);
  glTexCoord2d(0, 0);
  glVertex2f(0, 0);
  glTexCoord2d(1, 0);
  glVertex2f(1, 0);
  glTexCoord2d(1, 1);
  glVertex2f(1, 1);
  glTexCoord2d(0, 1);
  glVertex2f(0, 1);
  glEnd();
  glEnable(GL_DEPTH_TEST);

  glLoadIdentity();
  gluPerspective(60, (double)WIDTH / HEIGHT, 0.1, 1000.0);
  glMatrixMode(GL_MODELVIEW);

  // Camera
  float r = 8+5*(sin(time/1000.0));
  gluLookAt(r*sin(time/1000.0), r*cos(time/1000.0), r,
            0, 0, 2,
            0, 0, 1);

  // Light
  //GLfloat light_pos[4] = {-1.0, 0.0, 1.0, 1.0};
  //glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

  //GLfloat light_ambient[4] = {0.2, 0.2, 0.2, 1.0};
  //glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);

  GLfloat light_diffuse[4];
  light_diffuse[0] = b ? 0.4+(1-p)*0.5 : 0.4;
  light_diffuse[1] = b ? 0.4+(1-p)*0.6 : 0.4;
  light_diffuse[2] = b ? 0.4+(1-p)*0.7 : 0.4;
  light_diffuse[3] = 1.0;
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);

  glRotatef(angle, 0, 0, 1);

  if (PHASE_2)
  {
    if (p)
      angle += etime/BEAT_TIME * 90.0;
    glTranslatef(0, sin(time/20.0)*(b?1-p:0)*0.2, 0);
  }

  tree(time);
}

int display(uint time, uint etime, uint b, float p)
{
  if (PHASE_1)
    scene1(time, etime, b, p);
  else if (PHASE_2)
    scene1(time, etime, b, p);
  else // THE END
    return 1;

  return 0;
}
