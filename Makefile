NAME = demo
CC = gcc
CFLAGS = -std=c99 -Wall -Wextra -pedantic `sdl-config --cflags`

ifeq ($(shell uname), Darwin)
OS_FLAGS = -framework OpenGL
else # Assume Linux
OS_FLAGS = -lGL -lGLU
endif

LDFLAGS = -lm `sdl-config --libs` -lSDL_mixer -lGLEW -lSDL_image $(OS_FLAGS)

CFILES = demo.c shader.c scenes.c
CHDR = $(addprefix src/,shader.h scenes.h)
CSRC = $(CFILES:%c=src/%c)
OBJS = $(CFILES:%c=objs/%o)

.PHONY: clean distclean

all: $(NAME)

objs/%.o: src/%.c $(CHDR)
	$(CC) $(CFLAGS) -D DEBUG $< -c -o $@

objs:
	mkdir -p objs

$(NAME): objs $(OBJS)
	$(CC) $(OBJS) -o $(NAME) $(LDFLAGS)

clean:
	rm -rf objs/*.o core
	find . -name '*~' -exec rm -rf {} \; || true
	find . -name '#*' -exec rm -rf {} \; || true

distclean: clean
	rm -rf $(NAME) objs
